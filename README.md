# Resume Builder

### [Go to live demo](https://angular-app-0.herokuapp.com/register)

## Features
1.  User authentication/authorization.
2.  Resume Build and download.
3.  Edit Details.
4.  Inline editor for resume.

## MEAN Stack application

### Schema

    User: {
        name
        username : which would be unique
        password 
        email
        contact
    }

    Resume : schemaless
